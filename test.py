from itertools import izip
import numpy as np
from matplotlib import pyplot
import kwant
import green

def make_system(a=1, t=1.0, W=10, L=30):
    # Start with an empty tight-binding system and a single square lattice.
    # `a` is the lattice constant (by default set to 1 for simplicity.
    lat = kwant.lattice.square(a)

    syst = kwant.Builder()

    #### Define the scattering region. ####
    syst[(lat(x, y) for x in range(L) for y in range(W))] = 4 * t
    syst[lat.neighbors()] = -t

    #### Define and attach the leads. ####
    # Construct the left lead.
    lead = kwant.Builder(kwant.TranslationalSymmetry((-a, 0)))
    lead[(lat(0, j) for j in range(W))] = 4 * t
    lead[lat.neighbors()] = -t

    # Attach the left lead and its reversed copy.
    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())
    return syst


def singularities(system):
    bands = kwant.physics.Bands(system)
    ret = []
    ret.extend(bands(0))
    ret.extend(bands(np.pi))
    ret.sort()
    return ret


def test():
    syst = make_system(W=2, L=1).finalized()
    kwant.plotter.bands(syst.leads[0])

    points = singularities(syst.leads[0])

    G = green.green(syst, 2, points, [2., 3.], [0., 0.], [0.])
    Gl, Gu = G[0]
    np.testing.assert_almost_equal(-1j * (Gl - Gu), np.identity(len(Gl)))

if __name__ == "__main__":
    test()
