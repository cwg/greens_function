#!/bin/sh

test green.so -nt green.pyx && exit

cython --cplus green.pyx &&
    g++ -shared -pthread -fPIC -fwrapv -O3 -Wall -Wl,--no-as-needed -lgsl -lgslcblas -fno-strict-aliasing -I/usr/include/python2.7 -o green.so green.cpp &&
    python test.py
